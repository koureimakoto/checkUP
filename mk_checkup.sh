#!/bin/bash

#****************************************************************
#
#  CONNECTION CHECK - v.0.1
#
#  DEV_LIST::
#    >  v.0.2.1
#    >>  Implementar rotinas  para  multiplos  sites e  multiplos 
#       email.
#    >  v.0.3
#    >>  Implementar  versao de  configuracao  do cron  via  este 
#       script. Ex.: ./connection -C "*/5 * * *"
#
#
#  Verifica se o website caiu ou nao. Caso caia envia para o 
# email settado.
#
# $CHECK_URL    :: O URL alvo
#
# $CHECK_RESULT :: Retorno do alvo
#   >> Se nescitar visualizar o resultado da operação basta 
#   >> adicionar '-v' sem as simples. [não é o restorno]
#
# $RECV    :: O e-mail que irá receber o e-mail de Alerta
# $SEND_EMAIL :: E-mail que será usado para enviar
#   >> caso use um e-mail próprio na rotina do cron ignore
#
# $SEND_NAME  :: Nome de quem envia, complementa a header     
#
# $END       :: Uniao dos elementos do cabecalho para o element
#                elemento FROM, isto e, quem envia
#   >> nao settar nenhum valor para ele
#
#***************************************************************/


FROM_EMAIL=''

FROM_NAME=''

END=''

CHECK_URL=""

CHECK_RESULT=$(curl -sL -w "%{http_code}\n" $CHECK_URL -o /dev/null)

TO_EMAIL=""

# Se estiverem vazios, o cron usará email interno
if [[ ! -z $FROM_EMAIL ]] || [[ ! -z $FROM_NAME ]]; then
  FROM='-aFrom:${SFROM_NAME}\<$FROM_EMAIL\>' 
fi
 
#checa o resultado do cURL 
if [ $CHECK_RESULT -eq "200" ] ; then 
  echo "URL::($CHECK_URL) is UP : HTTP_RESULT::($CHECK_RESULT)" | \
       mail -s "STATUS OK" $END $TO_EMAIL
   echo "UP"
else
   echo "URL::($CHECK_URL) is DOWN : HTTP_RESULT::($CHECK_RESULT)"\
         | mail -s "ALERT"  $TO_EMAIL
   echo "DOWN"
fi
